CC=g++
CFLAGS=-c -Wall -std=c++11
LDFLAGS=-lpthread
CINCLUDE=-I./casablanca/Release/include/
CLIBFILES=-L./casablanca/Binaries/Release64/ -lcasablanca
CKILLWARNINGS=-Wno-reorder -Wno-unknown-pragmas
SOURCE_DIR=CrowdPathClientProgram/
SOURCE_FILES=$(wildcard $(SOURCE_DIR)/*.cpp)
OBJECTS=$(SOURCE_FILES:.cpp=.o)
BUILD_DIR=bin
EXECUTABLE=crowdpathclientprogram

all: checkout cpprest crowdPath

crowdPath: $(SOURCE_FILES) $(EXECUTABLE)
$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(CLIBFILES) $(LDFLAGS) 

.cpp.o:
	$(CC) $(CINCLUDE) $(CLIBFILES)  $(CFLAGS) $(CKILLWARNINGS) $< -o $@

checkout:
	@git submodule update --init --recursive

cpprest:
	cd casablanca/Release;make release
	$(MAKE) -C ./casablanca/Release/make release

clean:
	rm -rf $(SOURCE_DIR)/*.o $(EXECUTABLE)
	rm -rf $(BUILD_DIR)
	$(MAKE) -C casablanca/Release/make clean
