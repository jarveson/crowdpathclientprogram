#include "stdafx.h"
#include "ConfigManager.h"

std::map<std::string, std::string> ConfigManager::_logVariables;

ConfigManager::ConfigManager() {
	_fileDirectory = "";
	_fileName = "CrowdPathLog.txt";
}

ConfigManager::~ConfigManager(void) {
}

ConfigManager& ConfigManager::instance(){
	static ConfigManager config;
	config.LoadLog();
	return config;
}

bool ConfigManager::LoadLog(void) {
	if(_variablesLoaded){
		//ignore multiple calls
		return true;
	}
	std::ifstream logFile;
	std::string line;
	logFile.open(_fileDirectory + _fileName);
	if(logFile.is_open()) {
		while(logFile.good()) {
			std::getline (logFile, line);
			if (!ConfigManager::Parse(line)){
				//config is screwed up if this happens
				//should probly do more error checking than this
				//_logVariables.clear();
				logFile.close();
				return false;
			}
		}
		logFile.close();
		_variablesLoaded = true;

		return true;
	}
	else {
		return false;
	}
}

bool ConfigManager::Parse(std::string data) {
	if (data == ""){
		return true;
	}
	if (data.find(':') != std::string::npos){
		std::vector<std::string> tokens;
		tokens = split(data, ':');
		_logVariables.insert(std::pair<std::string, std::string>(tokens.at(0), tokens.at(1)));
		return true;
	}
	return false;
}

bool ConfigManager::WriteLog(void) {
	std::ofstream logFile;
	std::string line;
	logFile.open(_fileDirectory + _fileName, std::ios::trunc | std::ios::out);
	for (std::map<std::string, std::string>::const_iterator itr = _logVariables.begin();
		itr != _logVariables.end(); ++itr) {
		logFile << itr->first + ":" + itr->second + "\n";
	}
	logFile.close();
	return false;
}

bool ConfigManager::DataParsed(int numParsed) {
	if (ConfigManager::SetVariable<time_t>("LastParse", time(NULL))){
		ConfigManager::WriteLog();
	}
	int totalParsed = ConfigManager::GetTotalParsed();
	totalParsed += numParsed;
	if (ConfigManager::SetVariable<int>("TotalParsed", totalParsed)){
		return ConfigManager::WriteLog();
	}
	return false;
}
bool ConfigManager::ReceivedData(void) {
	if (ConfigManager::SetVariable<time_t>("LastUpdate", time(NULL))){
		return ConfigManager::WriteLog();
	}
	return false;
}

void ConfigManager::ResetCounter(void){
	if (ConfigManager::SetVariable<int>("TotalParsed", 0)){
		ConfigManager::WriteLog();
	}
}

int ConfigManager::GetTotalParsed(void){
	return GetVariable<int>("TotalParsed");
}

time_t ConfigManager::GetLastUpdate(void){
	return GetVariable<time_t>("LastUpdate");
}

time_t ConfigManager::GetLastParsing(void){
	return GetVariable<time_t>("LastParse");
}

std::string ConfigManager::GetWebSite(void){
	return GetVariable<std::string>("ServerAddress");
}

bool ConfigManager::SetWebsite(std::string website){
	if (ConfigManager::SetVariable<std::string>("ServerAddress", website)) {
		return ConfigManager::WriteLog();
	}
	return false;
}

bool ConfigManager::SetGUID(std::string guid){
	if (ConfigManager::SetVariable<std::string>("Guid", guid)) {
		return ConfigManager::WriteLog();
	}
	return false;
}
std::string ConfigManager::GetGuid(){
	return GetVariable<std::string>("Guid");
}

void ConfigManager::LoadDefaults(void) {
	_logVariables.insert(std::pair<std::string, std::string>("LastUpdate", "-1"));
	_logVariables.insert(std::pair<std::string, std::string>("LastParse", "-1"));
	_logVariables.insert(std::pair<std::string, std::string>("TotalParsed", "0"));
	_logVariables.insert(std::pair<std::string, std::string>("ServerAddress", "http://crowdpath.sturmh.net"));
	_logVariables.insert(std::pair<std::string, std::string>("Guid", "1234"));
	ConfigManager::WriteLog();
}

template<typename T>
T ConfigManager::GetVariable(std::string name){
	if (_variablesLoaded && !_logVariables.empty()){
		std::map<std::string, std::string>::const_iterator itr = _logVariables.find(name);
        if (itr == _logVariables.end()) {
			// not found...yey
			return NULL;
        }
		return ConfigManager::fromString<T>(itr->second);
	}
	// something broke?
	return NULL;
}

template<typename T>
bool ConfigManager::SetVariable(std::string name, T value){
	if (_variablesLoaded && !_logVariables.empty()) {
		std::map<std::string, std::string>::iterator itr = _logVariables.find(name);
		if(itr == _logVariables.end()) {
			return false;
		}
		itr->second = ConfigManager::toString<T>(value);
		return true;
	}
	return false;
}

std::vector<std::string> ConfigManager::split(std::string const& text, char sep){
    std::vector<std::string> tokens;
    size_t start = 0, end = 0;
    if ((end = text.find(sep, start)) != std::string::npos) {
        tokens.push_back(text.substr(start, end - start));
        start = end + 1;
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

template<typename T>
std::string ConfigManager::toString(const T &t) {
    std::ostringstream oss;
    oss << t;
    return oss.str();
}

template<typename T>
T ConfigManager::fromString( const std::string& s ) {
    std::istringstream stream( s );
    T t;
    stream >> t;
    return t;
}