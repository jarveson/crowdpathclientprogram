// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// im assuming this will also work in linux, might have to change when switching over

#include "targetver.h"

#include <string>
#include <vector>
#include <signal.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include <map>
#include <thread>

#ifdef _WIN32
#define printStringT std::wcout
#else
#define printStringT std::cout
#endif

