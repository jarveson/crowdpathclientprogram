#pragma once

#include "GpsPoint.h"

class GpsTrack {
private:
struct varStringNames {
		varStringNames()
			: gpsPoints(U("distance_matrix_entry")),
			  jobId(U("id"))
		{}
		utility::string_t jobId;
		utility::string_t gpsPoints;
	};
public:
	int jobId;
	std::vector<GpsPoint> gpsPoints;
	const varStringNames varNames;
	inline GpsTrack(int job, std::vector<GpsPoint> points) {jobId = job, gpsPoints = points;};
	void operator=(const GpsTrack&){};
};