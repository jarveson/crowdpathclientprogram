#pragma once
#include <cpprest/json.h>

class JsonRpc{
private:
	struct varStringNames {
		varStringNames()
			: jsonrpc(U("jsonrpc")), 
			  methodName(U("method")),
			  params(U("params")),
			  id(U("id"))
		{}
		utility::string_t jsonrpc;
		utility::string_t methodName;
		utility::string_t params;
		utility::string_t id;
	};
public:
	JsonRpc(void){};
	std::string jsonrpc;
	web::json::value params;
	std::string methodName;
	int id;
	const varStringNames varNames;
	void operator=(const JsonRpc &){};
};