#pragma once

// This file will include all the other json objects we may need.
// just include this file instead of all the others, they all sorta depend on each other anyway
#include "ParsedReturn.h"
#include "GpsTrack.h"
#include "GpsPoint.h"
#include "JsonRpc.h"