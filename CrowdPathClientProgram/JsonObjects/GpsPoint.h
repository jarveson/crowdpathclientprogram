#pragma once
class GpsPoint{
private:
	struct varStringNames {
		varStringNames()
			://pointNum(U("pointNum")),
			latitude(U("lat")),
			longitude(U("lon"))
		{}
		//utility::string_t pointNum;
		utility::string_t latitude;
		utility::string_t longitude;
	};
public:
	//int pointNum;
	double latitude;
	double longitude;
	const varStringNames varNames;
	inline GpsPoint(/*int pnum,*/ double lat, double lon) {/*pointNum = pnum; */latitude = lat; longitude = lon;}
	void operator=(const GpsPoint&){};
};