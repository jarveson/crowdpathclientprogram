#pragma once
#include <cpprest/json.h>

class ParsedReturn{
private:
	struct varStringNames {
		varStringNames()
			: googleJsonBlob(U("json_response")), 
			  seconds(U("duration")),
			  guid(U("workerId")),
			  jobId(U("id")),
			  timestamp(U("queryTime")),
			  polyline(U("polyline")),
			  distance(U("distance")),
			  destAddress(U("dest_address")),
			  origAddress(U("orig_address")),
			  directionJsonBlob(U("dir_json_response"))
		{}
		utility::string_t googleJsonBlob;
		utility::string_t seconds;
		utility::string_t guid;
		utility::string_t jobId;
		utility::string_t timestamp;
		utility::string_t polyline;
		utility::string_t distance;
		utility::string_t destAddress;
		utility::string_t origAddress;
		utility::string_t directionJsonBlob;
	};
public:
	ParsedReturn(void){};
	web::json::value googleJsonBlob;
	web::json::value directionJsonBlob;
	double seconds;
	time_t timestamp;
	std::string guid;
	utility::string_t polyline;
	utility::string_t destAddress;
	utility::string_t origAddress;
	int jobId;
	int distance;
	const varStringNames varNames;
	void operator=(const ParsedReturn &){};
};