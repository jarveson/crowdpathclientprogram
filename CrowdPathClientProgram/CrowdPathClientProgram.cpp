// CrowdPathClientProgram.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "WebDataManager.h"
#include "Thread.h"
#include "ConfigManager.h"

volatile bool running;

void signalHandler(int signal) {
	running = false;
	//kill warning 
	signal;
}

ThreadReturn inputLoop(void* webDataManager) {
	std::string command;
	
	while(true){
		//std::cout << "CrowdPath: ";
		getline(std::cin, command);
		//TODO: add more commands?
		if (command == ""){
			//std::cout << "CrowdPath: Unknown Command" << std::endl;
			continue;
		}
		else if (command == "quit"){
			((WebDataManager*)webDataManager)->runWebDataManager = false;
			break;
		}
		else if (command == "getjobs"){
			((WebDataManager*)webDataManager)->GetJobs();
		}
		else if (command == "parsejobs"){
			((WebDataManager*)webDataManager)->RequestFromGoogle(false);
		}
		else if (command == "sendjobs"){
			((WebDataManager*)webDataManager)->SendJobs();
		}
		else if (command == "clearjobs"){
			((WebDataManager*)webDataManager)->ClearJobs();
		}
		else if (command == "resetcounter"){
			((WebDataManager*)webDataManager)->ResetCounter();
		}
		// DEBUG
		else if (command =="resetjobs"){
			((WebDataManager*)webDataManager)->ResetJobs();
		}
	}

#ifdef _WIN32
    _endthread();
#else
    pthread_exit(NULL);
#endif
}

int main(int argc, char* argv[]) {
ConfigManager configManager = ConfigManager::instance();
#if _DEBUG
	configManager.SetGUID("1234");
	// killing warnings 
	argc;
	argv;
#else
	if (!(argc > 1)){
		std::cout << "CrowdPath: Expects Identifier string"<<std::endl;
		return 0;
	}
	configManager.SetGUID(argv[1]);
#endif
	WebDataManager webDataManager;
	//TODO: set up us the webDataManager
	
	//seed some random numbers up for later;
	srand((int)time(NULL));
	//start input thread
	Thread thread;
	thread.Start(&inputLoop, &webDataManager);
	if (true){ //if site exists, verify variables
		std::cout << "CrowdPath: Variables OK" << std::endl;
		if (configManager.LoadLog()) { //verify log file
			std::cout << "CrowdPath: Config File Valid" << std::endl;
		}
		else {
			std::cout << "CrowdPath: Config File Not Found/invalid, using defaults" << std::endl;
			configManager.LoadDefaults();
		}
		running = true;
		signal(SIGINT, signalHandler);
		std::cout << "CrowdPath: Running...type quit to quit" << std::endl;
		bool run = true;
		while (running && webDataManager.runWebDataManager){
			//do webdatamanagerstuff

			if (webDataManager.CanRequestFromGoogle()){
				run = true;
			}
			if (run == true){
				if (webDataManager.RequestFromGoogle(false)) {
					webDataManager.SendJobs();
				}
				else {
					run = false;
					std::cout << "Shutting down loop...type resetcounter to try again" <<std::endl;
				}
			}
			else {
				//nothing to do...slow down loop to avoid mashing cpu
				std::this_thread::sleep_for(std::chrono::milliseconds(1*2000));
			}
#if _DEBUG
			//slowing down debug..
			//limit our program to 1 request a second to avoid issues for now;
			//std::this_thread::sleep_for(std::chrono::milliseconds(1*1000));
#endif
		}
		//write stuff.
		configManager.WriteLog();
		std::cout << "CrowdPath: Closed" << std::endl;
	}

#if _DEBUG
	// keep command window up
	getchar();
#endif
	return 0;
}

