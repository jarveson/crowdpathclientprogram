#pragma once

class ConfigManager{

public:
	static ConfigManager& instance(); //singleton
	~ConfigManager(void);
	std::vector<std::string> split(std::string const&, char);
	bool LoadLog(void);
	bool WriteLog(void);
	time_t GetLastUpdate(void);
	time_t GetLastParsing(void);
	std::string GetWebSite(void);
	bool SetWebsite(std::string website);
	int GetTotalParsed(void);
	bool DataParsed(int numParsed);
	bool ReceivedData(void);
	bool SetGUID(std::string guid);
	std::string GetGuid();
	void LoadDefaults(void);
	void ResetCounter(void);
private:
	ConfigManager(void);
	std::string _fileName;
	std::string _fileDirectory;
	bool _variablesLoaded;
	static std::map<std::string, std::string> _logVariables;
	bool Parse(std::string data);
	template<typename T> T GetVariable(std::string name);
	template<typename T> bool SetVariable(std::string name, T value);
	template<typename T> static std::string toString(const T &t);
	template<typename T> static T fromString( const std::string& s);
};

