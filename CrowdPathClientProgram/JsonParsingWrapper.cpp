#include "stdafx.h"
#include "JsonParsingWrapper.h"
#include "ConfigManager.h"

 namespace convert = utility::conversions;

GpsPoint JsonParsingWrapper::GetGpsPoint(web::json::value jsonValue){
	//initialize some invalid values to check later
	GpsPoint point(91.0, 181.0);
	// not sure which one of these is better. im goin with the simpler, non loopy one

	/*for (auto iter = jsonValue.cbegin(); iter != jsonValue.cend(); ++iter){
		const web::json::value &str = iter->first;
		const web::json::value &v = iter->second;

		if (point.pointNum == -1 && str.as_string().compare(point.varNames.pointNum)==0){
			point.pointNum = v.as_integer();
		}

		else if (point.latitude == 181.0 && str.as_string().compare(point.varNames.latitude)==0){
			//probly should do some error checking?
			point.latitude = v.as_double();
		}

		else if (point.longitude == 91.0 && str.as_string().compare(point.varNames.longitude)==0){
			point.longitude = v.as_double();
		}
	}*/
	//point.pointNum = jsonValue.get(point.varNames.pointNum).as_integer();
	point.latitude = jsonValue.get(point.varNames.latitude).as_double();
	point.longitude = jsonValue.get(point.varNames.longitude).as_double();
	return point;
}

GpsTrack JsonParsingWrapper::GetGpsTrack(web::json::value jsonValue){
	std::vector<GpsPoint> points;
	GpsTrack track(-1, points);
	track.jobId = jsonValue.get(track.varNames.jobId).as_integer();

	std::vector<GpsPoint> tempPoints = JsonParsingWrapper::GetGpsPoints(jsonValue.get(track.varNames.gpsPoints));
	for (size_t x = 0; x<tempPoints.size();++x){
		points.push_back(tempPoints[x]);
	}
	//track.jobId = jsonValue.get(track.varNames.jobId).as_integer();
	track.gpsPoints = points;
	return track;
}

std::vector<GpsPoint> JsonParsingWrapper::GetGpsPoints(web::json::value jsonValue){
	//hard code this cause it keeps changing
	std::vector<GpsPoint> points;

	GpsPoint point(91,181);
	point.latitude = jsonValue.get(U("origin")).get(point.varNames.latitude).as_double();
	point.longitude = jsonValue.get(U("origin")).get(point.varNames.longitude).as_double();
	points.push_back(point);

	point.latitude = jsonValue.get(U("dest")).get(point.varNames.latitude).as_double();
	point.longitude = jsonValue.get(U("dest")).get(point.varNames.longitude).as_double();
	points.push_back(point);

	return points;
}

std::vector<GpsTrack> JsonParsingWrapper::GetGpsTracks(web::json::value jsonValue) {
	std::vector<GpsTrack> tracks;

	/*for (auto iter = jsonValue.cbegin(); iter != jsonValue.cend(); ++iter) {
		const web::json::value &str = iter->first;
		const web::json::value &v = iter->second;
		if (str.as_string() == U("id")){
			tracks.push_back(JsonParsingWrapper::GetGpsTrack(iter->));
		}
	}*/

	if (jsonValue.type() == web::json::value::Array){
		for (size_t x=0;x<jsonValue.size();++x){
			tracks.push_back(JsonParsingWrapper::GetGpsTrack(jsonValue[x]));
		}
	}
	else {
		tracks.push_back(JsonParsingWrapper::GetGpsTrack(jsonValue));
	}
	return tracks;
}

void JsonParsingWrapper::PrintJson(web::json::value val, utility::string_t name){
	printStringT << L"Json Dump of :" << name <<std::endl;
	for (auto iter = val.cbegin(); iter != val.cend(); ++iter){
		const web::json::value &str = iter->first;
		const web::json::value &v = iter->second;
		if (typeid(&v) == typeid(web::json::value::array())){
			JsonParsingWrapper::PrintJson(v, str.as_string());
		}
		printStringT << L"String: "<< str.as_string() << L", Value: " << v.to_string() << std::endl;
	}
}

std::string JsonParsingWrapper::FormatGoogleParams( GpsTrack job){
	std::stringstream ss;
	GpsPoint origin = job.gpsPoints[0];
	GpsPoint dest = job.gpsPoints[1];
	ss.precision(10);
	ss << "?origins=" << origin.latitude << "," << origin.longitude;
	ss << "&destinations=" << dest.latitude << "," << dest.longitude;
	ss << "&sensor=false&languange=en";
	return ss.str();
}

std::string JsonParsingWrapper::FormatGoogleDirectionParams( GpsTrack job){
	std::stringstream ss;
	GpsPoint origin = job.gpsPoints[0];
	GpsPoint dest = job.gpsPoints[1];
	ss.precision(10);
	ss << "?origin=" << origin.latitude << "," << origin.longitude;
	ss << "&destination=" << dest.latitude << "," << dest.longitude;
	ss << "&sensor=false&languange=en";
	return ss.str();
}

std::string JsonParsingWrapper::GetJobsFromServer(int numJobs, std::string guid){
	//web::json::value returnJson;
	//web::json::value paramsObj;
	//JsonRpc jsonRpc;

	//paramsObj[L"clientId"] = web::json::value::string(convert::to_string_t(guid));
	//paramsObj[L"numJobs"] = 2;

	//returnJson[jsonRpc.varNames.params] = paramsObj;
	//returnJson[jsonRpc.varNames.id] = jobId;
	//returnJson[jsonRpc.varNames.methodName] = web::json::value::string(U("requestJobs"));
	//returnJson[jsonRpc.varNames.jsonrpc] = web::json::value::string(U("2.0"));

	std::stringstream ss;
	ss << "getJobs?numJobs=" << numJobs;
	ss << "&workerId=" << guid;

	return ss.str();
}

web::json::value JsonParsingWrapper::JsonRpcSendParsedToServer(web::json::value distanceMatrix, web::json::value directionBlob,int jobId){
	//just wraps other calls in json rpc thing
	//get the json object

	/*JsonRpc returnRpc;
	returnRpc.params = JsonParsingWrapper::FormatForServer(jsonValue, jobId);
	returnRpc.id = rpcId;
	returnRpc.jsonrpc = "2.0";
	returnRpc.methodName = "completeJobs";*/

	/*web::json::value returnJson;
	JsonRpc jsonRpc;
	returnJson[jsonRpc.varNames.params] = JsonParsingWrapper::FormatForServer(jsonValue, jobId);
	returnJson[jsonRpc.varNames.id] = web::json::value::string(convert::to_string_t(guid));
	returnJson[jsonRpc.varNames.methodName] = web::json::value::string(U("2.0"));
	returnJson[jsonRpc.varNames.jsonrpc] = web::json::value::string(U("completeJobs"));

	return returnJson;*/

	return FormatForServer(distanceMatrix, directionBlob, jobId);
}

web::json::value JsonParsingWrapper::FormatForServer(web::json::value distanceMatrix, web::json::value directionsBlob,int jobId){
	//this should work...hopefully to get seconds
	int seconds = distanceMatrix[U("rows")][0][U("elements")][0][U("duration")][U("value")].as_integer();
	ParsedReturn parsedReturn;
	
	parsedReturn.googleJsonBlob = distanceMatrix;
	parsedReturn.distance = distanceMatrix[U("rows")][0][U("elements")][0][U("distance")][U("value")].as_integer();
	parsedReturn.guid = ConfigManager::instance().GetGuid();
	parsedReturn.jobId = jobId;
	parsedReturn.seconds = seconds;
	parsedReturn.timestamp = time(NULL);
	parsedReturn.origAddress = distanceMatrix[U("origin_address")][0].to_string();
	parsedReturn.destAddress = distanceMatrix[U("destination_address")][0].to_string();
	parsedReturn.polyline = directionsBlob[U("routes")][0][U("overview_polyline")][U("points")].to_string();
	parsedReturn.directionJsonBlob = directionsBlob;
	
	return JsonParsingWrapper::JsonParsedReturn(parsedReturn);
}

// private function, just a helper function for FormatForServer
web::json::value JsonParsingWrapper::JsonParsedReturn(ParsedReturn parsedReturn){
	web::json::value returnObj;
	int workerid;
	std::istringstream(parsedReturn.guid) >> workerid;
	returnObj[parsedReturn.varNames.googleJsonBlob] = parsedReturn.googleJsonBlob;
	returnObj[parsedReturn.varNames.guid] = web::json::value::number(workerid);
	returnObj[parsedReturn.varNames.jobId] = web::json::value::number(parsedReturn.jobId);
	returnObj[parsedReturn.varNames.seconds] = web::json::value::number(parsedReturn.seconds);
	returnObj[parsedReturn.varNames.polyline] = web::json::value::string(parsedReturn.polyline);
	returnObj[parsedReturn.varNames.distance] = parsedReturn.distance;
	returnObj[parsedReturn.varNames.directionJsonBlob] = parsedReturn.directionJsonBlob;
	std::ostringstream oss;
	oss << parsedReturn.timestamp;
	returnObj[parsedReturn.varNames.timestamp] = web::json::value::string(convert::to_string_t(oss.str()));
	std::string time=oss.str();
	return returnObj;
}