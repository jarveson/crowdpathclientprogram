#pragma once
#include "JsonObjects/JsonObjects.h"
#include <cpprest/json.h>

class JsonParsingWrapper
{
public:
	static std::vector<GpsTrack> GetGpsTracks(web::json::value jsonValue);
	// this formats a job object in queue for use with sending to google for distance request
	static std::string FormatGoogleParams(GpsTrack job);
	static std::string FormatGoogleDirectionParams(GpsTrack job);
	//this converts google parse response into a ready to send json-rpc wrapped object
	static web::json::value JsonRpcSendParsedToServer(web::json::value distanceMatrix, web::json::value directionBlob, int jobId);
	static std::string GetJobsFromServer(int numJobs, std::string guid);
	static void PrintJson(web::json::value val, utility::string_t name);
private:
	// these take raw json blob and convert to gpspoint, make sure the json is right...
	// otherwise weird things will probably happen.
	static std::vector<GpsPoint> GetGpsPoints(web::json::value jsonValue);
	static GpsPoint GetGpsPoint(web::json::value jsonValue);
	static GpsTrack GetGpsTrack(web::json::value jsonValue);
	// this is just a helper function that takes a parsed return object and returns the json for it.
	static web::json::value JsonParsedReturn(ParsedReturn parsedReturn);
	// this is the wrapper/call to use to return to server with a job
	static web::json::value FormatForServer(web::json::value distanceMatrix, web::json::value directionBlob, int jobId);

};

