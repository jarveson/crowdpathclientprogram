#pragma once

#include "JsonObjects/JsonObjects.h"

class SendQueue{
public:
	web::json::value distanceMatrix;
	web::json::value directionBlob;
	int jobId;
};


class WebDataManager
{
public:
	bool runWebDataManager;
	WebDataManager(void){runWebDataManager = true;};
	// main call that will handle logic / checking so we dont hit 
	// google api cap
	bool RequestFromGoogle(bool sendJobs=true);
	void ResetJobs();
	static std::vector<GpsTrack> jobQueue;
	static std::vector<SendQueue> sendQueue;
	void SendJobs(void);
	void GetJobs(void);
	void ClearJobs(void);
	void ResetCounter(void);
	bool CanRequestFromGoogle(void);
private:
	static int jsonIdNumber;
	web::json::value googleReturn();
	static pplx::task<void> GetJobsFromServer(int numJobs=5);
	static pplx::task<void> ParseFromGoogle(void);
	static pplx::task<void> ParsePolyLineFromGoogle();
	static pplx::task<void> SendToServer();
	void AtMaxJobs();
};
