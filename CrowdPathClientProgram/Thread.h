#pragma once

// Simple thread object to deal with windows vs linux calls
// this probably can only be called once in the program cause id is hard coded to '0' in code

#ifdef _WIN32

#include <process.h>
typedef void (*ThreadFunction)(void* param);
typedef void ThreadReturn;
typedef int ThreadId;

#else

#include "pthread.h"
typedef void* (*ThreadFunction)(void* param);
typedef void* ThreadReturn;
typedef pthread_t ThreadId;

#endif

class Thread
{
private:
    ThreadId _threadId;

public:
    Thread(void);
    ~Thread(void);

    bool Start(ThreadFunction callback, void* param);
};

