#include "stdafx.h"
#include "WebDataManager.h"
#include <cpprest/http_client.h>
#include "JsonParsingWrapper.h"
#include "ConfigManager.h"

using namespace web;
using namespace web::http;
using namespace web::http::client;
namespace convert = utility::conversions;

std::vector<GpsTrack> WebDataManager::jobQueue;
std::vector<SendQueue> WebDataManager::sendQueue;

bool runWebDataManager = true;
int WebDataManager::jsonIdNumber;

bool WebDataManager::RequestFromGoogle(bool sendJobs) {

	//first see if our jobqueue is empty
	if(jobQueue.empty()){
		//not doing this async
		WebDataManager::GetJobsFromServer().wait();
		if(jobQueue.empty()){
			std::cout<<"Unable to get jobs"<<std::endl;
			return false;
		}
	}
	ConfigManager config = ConfigManager::instance();
	while (jobQueue.size() > 0){
		if(WebDataManager::CanRequestFromGoogle()){
			WebDataManager::ParseFromGoogle().wait();
			std::this_thread::sleep_for(std::chrono::milliseconds(1*250));
			config.DataParsed(1);
		}
		else {
			std::cout<<"At max Requests!"<<std::endl;
			return false;
		}
	}
	if (sendJobs){
		SendJobs();
	}
	return true;
	//limit our program to 1 request a second to avoid issues for now;
	//std::this_thread::sleep_for(std::chrono::milliseconds(1*1000));
}

void WebDataManager::GetJobs(void) {
	//first see if our jobqueue is empty
	if(jobQueue.empty()){
		//not doing this async
		WebDataManager::GetJobsFromServer().wait();
		if(jobQueue.empty()){
			std::cout<<"Unable to get jobs"<<std::endl;
			return;
		}
	}
}

void WebDataManager::ClearJobs(void){
	jobQueue.clear();
	sendQueue.clear();
}

void WebDataManager::SendJobs(void){
	//for (unsigned int x=0;x<sendQueue.size();++x){
	while(sendQueue.size() > 0){
		WebDataManager::SendToServer().wait();
	}
}

void WebDataManager::AtMaxJobs(void){
	ConfigManager config = ConfigManager::instance();
	config.DataParsed(2500);
}

void WebDataManager::ResetCounter(void){
	ConfigManager config = ConfigManager::instance();
	config.ResetCounter();
}

pplx::task<void> WebDataManager::GetJobsFromServer(int numJobs){
	ConfigManager config = ConfigManager::instance();
	http_client client(convert::to_string_t(ConfigManager::instance().GetWebSite()));
	
	//jsonIdNumber = rand() % 100 + 1;
	std::string sendParams = JsonParsingWrapper::GetJobsFromServer(numJobs, config.GetGuid());
	sendParams += "&worker_id=1234";
	//JsonParsingWrapper::JsonRpcSendParsedToServer(jsonValue, jobQueue.back().jobId, jsonIdNumber);
	std::string relativeURL = "/api/" + sendParams;
	utility::string_t convertedRelativeURL = utility::conversions::to_string_t(relativeURL);
	return client.request(methods::GET, convertedRelativeURL).then([](http_response response) -> pplx::task<json::value>
	{
		if(response.status_code() == status_codes::OK){
			return response.extract_json();
		}

		return response.extract_json();

		//TODO:: make sure that if it errors out, that its actually json
		// probly try/catch?
		//return pplx::task_from_result(json::value());
	})
		.then([](pplx::task<json::value> previousTask)
	{
		try {
			const json::value& v = previousTask.get();
			if(v.type() == json::value::Null){
				throw;
			}
			//process json value
			if (v.type() == json::value::Array || (v.type() == json::value::String && v[0].as_string() == U("id"))){
				//we got either an array or a single job
				//send it to funtion to process either.
				std::vector<GpsTrack> newTracks = JsonParsingWrapper::GetGpsTracks(v);
				std::cout << "Got " << newTracks.size() << " job(s) from server!"<<std::endl;
				for (unsigned int x=0;x<newTracks.size();++x){
					jobQueue.push_back(newTracks[x]);
				}
			}
			else {
				//bleh, what broke
				std::cout << "CrowdPath: Retrieve Jobs Failure!";
				if (v[1].type() == json::value::String){
					printStringT << "Possible Json Error: " << v[1][0].to_string() << " : " << v[1][1].as_string() << std::endl;
				}
				else {
					std::cout << std::endl;
				}
			}
		}
		catch (const std::exception& e){
			std::wostringstream ss;
			ss << e.what() << std::endl;
			std::wcout << ss.str();
		}
	});
}

pplx::task<void> WebDataManager::ParseFromGoogle(void) {
	http_client client(U("http://maps.googleapis.com/maps/api/distancematrix/json"));
	std::string params = JsonParsingWrapper::FormatGoogleParams(jobQueue.back());
	return client.request(methods::GET, utility::conversions::to_string_t(params)).then([](http_response response) -> pplx::task<json::value>
	{
		if(response.status_code() == status_codes::OK){
			return response.extract_json();
		}

		//error cases
		return pplx::task_from_result(json::value());
	})
		.then([](pplx::task<json::value> previousTask)
	{
		try {
			const json::value& v = previousTask.get();
			//process json value
			if (v.size() == 0){
				std::cout << "Website connection error!"<<std::endl;
			}
			else if (v.get(U("status")).as_string() == U("OK")){
				//Good! send it out
				//WebDataManager::SendToServer(v).wait();
				SendQueue queue;
				queue.distanceMatrix = v;
				sendQueue.push_back(queue);
				WebDataManager::ParsePolyLineFromGoogle().wait();
				std::cout <<"Job Parsed!"<<std::endl;
				//jobQueue.pop_back();
			}
		}
		catch (const std::exception& e){
			std::wostringstream ss;
			ss << e.what() << std::endl;
			std::wcout << ss.str();
			//ut oh, kill the loop

		}
	});
}

pplx::task<void> WebDataManager::ParsePolyLineFromGoogle(){
	http_client client(U("http://maps.googleapis.com/maps/api/directions/json"));
	std::string params = JsonParsingWrapper::FormatGoogleDirectionParams(jobQueue.back());
	return client.request(methods::GET, utility::conversions::to_string_t(params)).then([](http_response response) -> pplx::task<json::value>
	{
		if(response.status_code() == status_codes::OK){
			return response.extract_json();
		}

		//error cases
		return pplx::task_from_result(json::value());
	})
		.then([](pplx::task<json::value> previousTask)
	{
		try {
			const json::value& v = previousTask.get();
			//process json value
			if (v.size() == 0){
				std::cout << "Website connection error!";
			}
			else if (v.get(U("status")).as_string() == U("OK")){
				//Good! send it out
				sendQueue.back().directionBlob = v;
				sendQueue.back().jobId = jobQueue.back().jobId;
				jobQueue.pop_back();
				//WebDataManager::SendToServer().wait();
				//jobQueue.pop_back();
			}
		}
		catch (const std::exception& e){
			std::wostringstream ss;
			ss << e.what() << std::endl;
			std::wcout << ss.str();
		}
	});
}

bool WebDataManager::CanRequestFromGoogle(){
	ConfigManager config = ConfigManager::instance();
	time_t lastUpdate = config.GetLastUpdate();
	double difTime = difftime(time(NULL), lastUpdate);
	difTime = ((difTime/60.0) / 60.0);
	if (difTime > 24){
		config.ReceivedData();
	}
	else if (config.GetTotalParsed() >= 2400){
		return false;
	}
	return true;
}

pplx::task<void> WebDataManager::SendToServer() {
	ConfigManager config = ConfigManager::instance();
	http_client client(convert::to_string_t(ConfigManager::instance().GetWebSite()));
	//jsonIdNumber = rand() % 100 + 1;
	SendQueue queue = sendQueue.back();
	web::json::value sendData = JsonParsingWrapper::JsonRpcSendParsedToServer(queue.distanceMatrix, queue.directionBlob, queue.jobId);
	return client.request(methods::POST, U("/api/completeJob"), sendData).then([](http_response response) -> pplx::task<json::value>
	{
		if(response.status_code() == status_codes::OK){
			// not sure what will get returned
			return response.extract_json();
		}

		//error cases
		std::cout << "CrowdPath: Send Jobs Failure: Returned code: "<<response.status_code()<<std::endl;
		printStringT << response.to_string();
		return pplx::task_from_result(json::value());
	})
		.then([](pplx::task<json::value> previousTask)
	{
		try {
			
			const json::value& v = previousTask.get();
			//if (v[0].as_string() == U("2.0") && v[1].as_string() == U("*/
			//printStringT << v.to_string() <<std::endl;
			if (v.type() == json::value::Null){
				std::cout << "CrowdPath: Send Jobs Failure! No json returned!"<<std::endl;
			}
			else if (v.size() > 1){
				//error
				std::cout << "CrowdPath: Send Jobs Failure!"<<std::endl;
				printStringT << "Possible Json Error: " << v[U("message")].as_string() << std::endl;

				if (v[U("error")].as_integer() == 2){
					//if could not find job error, just pop it from stack and keep goin
					//jobQueue.pop_back();
					sendQueue.pop_back();
				}
			}
			else if (v[U("success")].as_string() == U("true")){
				//jobQueue.pop_back();
				sendQueue.pop_back();
				std::cout << "Job Processed!"<<std::endl;
			}
			//return; 
			//process json value...wait what do we process? just go on with our lives..
		}
		catch (const std::exception& e){
			std::wostringstream ss;
			ss << e.what() << std::endl;
			std::wcout << ss.str();
		}
	});
}

//DEBUG function.....quick hack/one-off function
void WebDataManager::ResetJobs(){
	jobQueue.clear();
	/*web::json::value returnJson;
	web::json::value paramsObj;
	JsonRpc jsonRpc;

	paramsObj[0] = web::json::value();
	//paramsObj[L"numJobs"] = 2;

	returnJson[jsonRpc.varNames.params] = paramsObj;
	returnJson[jsonRpc.varNames.id] = rand() % 100 + 1;
	returnJson[jsonRpc.varNames.methodName] = web::json::value::string(U("reset"));
	returnJson[jsonRpc.varNames.jsonrpc] = web::json::value::string(U("2.0"));
	*/
	ConfigManager config = ConfigManager::instance();
	http_client client(convert::to_string_t(ConfigManager::instance().GetWebSite()));
	client.request(methods::GET, U("/api/reset"));
}