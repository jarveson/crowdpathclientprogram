----Crowdpath client 
	for use with CrowdPath server

Grabs google requests based on jobs given from server, parses and returns

Currently will support win32 and linux. Will eventually just release binarys instead of having to build

Uses casablanca c++ rest sdk libaray for json/rest calls
found here: http://casablanca.codeplex.com/

----Running

Grab CrowdPathClientProgram.exe along with casablanca1xx.dll

Run it: CrowdPathClientProgram <guid>

----Installing/building windows: 

Currently only tested on windows 7, should work for any other variant, however.

Clone repo, Master should eventually be better than develop

Use/install Visual Studio 2012 - Might need full rather than express, if anything grab c++ studio express

Open solution and build release, debug will not function fully

----Installing/building Linux:

Tested on Ubuntu 12.10, should work on newer, other flavors of linux will need to adapt some of these commands.

clone repo, master should be better than develop

sudo apt-get install g++ libxml++2.6-dev libboost-all-dev 

currently casablanca unittests error out when building but doesnt affect us, so use 'make' all until casablanca errors, 
then use 'make crowdpath' to build up crowdpath

